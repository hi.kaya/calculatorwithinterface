public interface ICalculate {

        double operation(double firstNumber, double secondNumber);

}
class Add implements ICalculate
{
        @Override
        public double operation(double firstNumber, double secondNumber)
        {
                return firstNumber+secondNumber;
        }
}

class Substract implements ICalculate
{
        @Override
        public double operation(double firstNumber, double secondNumber)
        {
                return firstNumber-secondNumber;
        }

}
class Multiply implements ICalculate
{
        @Override
        public double operation(double firstNumber, double secondNumber)
        {
                return firstNumber*secondNumber;
        }
}
class Divided implements ICalculate
{
        @Override
        public double operation(double firstNumber, double secondNumber)
        {
                return firstNumber/secondNumber;
        }
}


