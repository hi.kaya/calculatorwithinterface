import java.util.HashMap;

public class Main
{
    private static HashMap<String, ICalculate> operations = new HashMap<>();
    public static void main(String [] args)
    {

        operations.put("+", new Add());
        operations.put("-", new Substract());
        operations.put("*", new Multiply());
        operations.put("/", new Divided());


        Offer offerObject = new Offer();
        offerObject.showInstruction();
        String operator=offerObject.getOperation();
        double number1 = offerObject.getNumber();
        double number2 = offerObject.getNumber();
        System.out.println(operations.get(operator).operation(number1, number2));

    }
}
