import java.util.Scanner;
public class Offer {

    Scanner scanner=new Scanner(System.in);
    public void showInstruction(){
        System.out.println("Operation: \n" +
                "+ \n" +
                "- \n" +
                "* \n" +
                "/ \n");
    }
    public String getOperation()
    {
        System.out.println("Enter operation :");
        return scanner.nextLine();
    }

    public double getNumber() {
        System.out.println("Enter number: ");
        return scanner.nextDouble();
    }

}
